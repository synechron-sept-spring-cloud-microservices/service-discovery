# Service Discovery

## Steps for Service discovery

### Dependencies 
```yml
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-starter-bootstrap</artifactId>
    </dependency>
```

### Bootstrap configuration
```yml
spring:
  application:
    name: eurekaservice
```

### Application configuration
```yml
server:
  port: 8761

eureka:
  server:
    enable-self-preservation: false
  instance:
    lease-renewal-interval-in-seconds: 30
    lease-expiration-duration-in-seconds: 90

  client:
    register-with-eureka: false
    fetch-registry: false
```

### Enable Eureka server annotation
```java
@SpringBootApplication
@EnableEurekaServer
public class DiscoveryServiceApplication ...
```

### Testing
1. Start the server on `8761` port
2. Verify that the server is running and displaying the Eureka dashboard on `http://localhost:8761`.

